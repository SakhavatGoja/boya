<?php
    include("includes/head.php");
?>


<section class="favourites">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
        <?php
            include("includes/category.list.php");
        ?>
        <div class="container">
          <div class="row">
              <div class="favourites_main_container">
                  <div class="breadcrumbs">
                      <span class="old_page">Azclimart</span>
                      <img src="img/breadcrumb.svg" alt="">
                      <a href="#" class="new_page">Bəyəndiklərim</a>
                  </div>

                  <div class="heading_container_same">
                      <p class="title_same_heading">Bəyəndiklərim</p>
                  </div>

                  <div class="favourites_boxes">
                    <div class="favourite_single">
                      <div class="paint_box">
                        <div class="discount">
                            <p>45%</p>
                        </div>
                        <button class="heart active">
                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                            </svg>
                        </button>
                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                        <div class="paint_desc">
                            <p class="paint_firm">Corella boyaları</p>
                            <p class="paint_name">Emulsiyalar</p>
                            <p class="paint_type">Super Plus</p>
                        </div>
                        <div class="paint_prices">
                            <div class="price_box">
                                <p class="current_price">75.80₼</p>
                                <p class="old_price">63.50₼</p>
                            </div>
                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                        </div>
                      </div>
                      <button class="delete">
                        <img src="img/esc.svg" alt="">
                      </button>
                    </div>
                    <div class="favourite_single">
                      <div class="paint_box">
                        <div class="discount">
                            <p>45%</p>
                        </div>
                        <button class="heart active">
                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                            </svg>
                        </button>
                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                        <div class="paint_desc">
                            <p class="paint_firm">Corella boyaları</p>
                            <p class="paint_name">Emulsiyalar</p>
                            <p class="paint_type">Super Plus</p>
                        </div>
                        <div class="paint_prices">
                            <div class="price_box">
                                <p class="current_price">75.80₼</p>
                                <p class="old_price">63.50₼</p>
                            </div>
                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                        </div>
                      </div>
                      <button class="delete">
                        <img src="img/esc.svg" alt="">
                      </button>
                    </div>
                    <div class="favourite_single">
                      <div class="paint_box">
                        <div class="discount">
                            <p>45%</p>
                        </div>
                        <button class="heart active">
                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                            </svg>
                        </button>
                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                        <div class="paint_desc">
                            <p class="paint_firm">Corella boyaları</p>
                            <p class="paint_name">Emulsiyalar</p>
                            <p class="paint_type">Super Plus</p>
                        </div>
                        <div class="paint_prices">
                            <div class="price_box">
                                <p class="current_price">75.80₼</p>
                                <p class="old_price">63.50₼</p>
                            </div>
                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                        </div>
                      </div>
                      <button class="delete">
                        <img src="img/esc.svg" alt="">
                      </button>
                    </div>
                    <div class="favourite_single">
                      <div class="paint_box">
                        <div class="discount">
                            <p>45%</p>
                        </div>
                        <button class="heart active">
                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                            </svg>
                        </button>
                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                        <div class="paint_desc">
                            <p class="paint_firm">Corella boyaları</p>
                            <p class="paint_name">Emulsiyalar</p>
                            <p class="paint_type">Super Plus</p>
                        </div>
                        <div class="paint_prices">
                            <div class="price_box">
                                <p class="current_price">75.80₼</p>
                                <p class="old_price">63.50₼</p>
                            </div>
                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                        </div>
                      </div>
                      <button class="delete">
                        <img src="img/esc.svg" alt="">
                      </button>
                    </div>
                    <div class="favourite_single">
                      <div class="paint_box">
                        <div class="discount">
                            <p>45%</p>
                        </div>
                        <button class="heart active">
                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                            </svg>
                        </button>
                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                        <div class="paint_desc">
                            <p class="paint_firm">Corella boyaları</p>
                            <p class="paint_name">Emulsiyalar</p>
                            <p class="paint_type">Super Plus</p>
                        </div>
                        <div class="paint_prices">
                            <div class="price_box">
                                <p class="current_price">75.80₼</p>
                                <p class="old_price">63.50₼</p>
                            </div>
                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                        </div>
                      </div>
                      <button class="delete">
                        <img src="img/esc.svg" alt="">
                      </button>
                    </div>
                    <div class="favourite_single">
                      <div class="paint_box">
                        <div class="discount">
                            <p>45%</p>
                        </div>
                        <button class="heart active">
                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                            </svg>
                        </button>
                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                        <div class="paint_desc">
                            <p class="paint_firm">Corella boyaları</p>
                            <p class="paint_name">Emulsiyalar</p>
                            <p class="paint_type">Super Plus</p>
                        </div>
                        <div class="paint_prices">
                            <div class="price_box">
                                <p class="current_price">75.80₼</p>
                                <p class="old_price">63.50₼</p>
                            </div>
                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                        </div>
                      </div>
                      <button class="delete">
                        <img src="img/esc.svg" alt="">
                      </button>
                    </div>
                    <div class="favourite_single">
                      <div class="paint_box">
                        <div class="discount">
                            <p>45%</p>
                        </div>
                        <button class="heart active">
                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                            </svg>
                        </button>
                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                        <div class="paint_desc">
                            <p class="paint_firm">Corella boyaları</p>
                            <p class="paint_name">Emulsiyalar</p>
                            <p class="paint_type">Super Plus</p>
                        </div>
                        <div class="paint_prices">
                            <div class="price_box">
                                <p class="current_price">75.80₼</p>
                                <p class="old_price">63.50₼</p>
                            </div>
                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                        </div>
                      </div>
                      <button class="delete">
                        <img src="img/esc.svg" alt="">
                      </button>
                    </div>
                    <div class="favourite_single">
                      <div class="paint_box">
                        <div class="discount">
                            <p>45%</p>
                        </div>
                        <button class="heart active">
                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                            </svg>
                        </button>
                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                        <div class="paint_desc">
                            <p class="paint_firm">Corella boyaları</p>
                            <p class="paint_name">Emulsiyalar</p>
                            <p class="paint_type">Super Plus</p>
                        </div>
                        <div class="paint_prices">
                            <div class="price_box">
                                <p class="current_price">75.80₼</p>
                                <p class="old_price">63.50₼</p>
                            </div>
                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                        </div>
                      </div>
                      <button class="delete">
                        <img src="img/esc.svg" alt="">
                      </button>
                    </div>
                    <div class="favourite_single">
                      <div class="paint_box">
                        <div class="discount">
                            <p>45%</p>
                        </div>
                        <button class="heart active">
                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                            </svg>
                        </button>
                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                        <div class="paint_desc">
                            <p class="paint_firm">Corella boyaları</p>
                            <p class="paint_name">Emulsiyalar</p>
                            <p class="paint_type">Super Plus</p>
                        </div>
                        <div class="paint_prices">
                            <div class="price_box">
                                <p class="current_price">75.80₼</p>
                                <p class="old_price">63.50₼</p>
                            </div>
                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                        </div>
                      </div>
                      <button class="delete">
                        <img src="img/esc.svg" alt="">
                      </button>
                    </div>
                    <div class="favourite_single">
                      <div class="paint_box">
                        <div class="discount">
                            <p>45%</p>
                        </div>
                        <button class="heart active">
                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                            </svg>
                        </button>
                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                        <div class="paint_desc">
                            <p class="paint_firm">Corella boyaları</p>
                            <p class="paint_name">Emulsiyalar</p>
                            <p class="paint_type">Super Plus</p>
                        </div>
                        <div class="paint_prices">
                            <div class="price_box">
                                <p class="current_price">75.80₼</p>
                                <p class="old_price">63.50₼</p>
                            </div>
                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                        </div>
                      </div>
                      <button class="delete">
                        <img src="img/esc.svg" alt="">
                      </button>
                    </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
