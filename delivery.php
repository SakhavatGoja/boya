<?php
    include("includes/head.php");
?>


<section class="delivery">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
        <?php
            include("includes/category.list.php");
        ?>
        <div class="container">
            <div class="row">
                <div class="delivery_main_container">
                    <div class="breadcrumbs">
                        <span class="old_page">Azclimart</span>
                        <img src="img/breadcrumb.svg" alt="">
                        <a href="#" class="new_page">Məkana özəl məhsullarımız</a>
                    </div>
                    <div class="heading_container_same">
                        <p class="title_same_heading">Çatdırılma</p>
                    </div>
                    <div class="delivery_info_container">
                      <p class="same_blue_title">Çatdırılma şərtləri və müddəti:</p>
                      <div class="mutual_delivery_info_box">
                        <div class="mutual_delivery_single">
                          <div class="mutual_heading">
                            <p class="mutual_title">Bakı şəhəri üzrə:</p>                            
                          </div>
                          <p class="delivery_text">
                            Bakı şəhəri üzrə çatdırılma sizin üçün münasib ünvana, metro stansiyasına və ya istənilən digər orientir məkana həyata keçirilir. Nəzərə almağınızı xahiş edirik ki, çatdırılma qapıya qədər deyil, binanın, ticarət mərkəzinin və ya digər çatdırılma məkanının əsas girişinə qədər həyata keçirilir.
                            Əgər sifariş 12:00-dək edilibsə, bu zaman çatdırılma gün ərzində Bakı vaxtı ilə 21:00-dək həyata keçirilir; əgər sifariş günün ikinci yarısında edilibsə, bu zaman çatdırılma cari sifarişlərin miqdarından asılı olaraq həmin gün ərzində, və ya  növbəti gün həyata keçirilə bilər. İstəyinizə görə, menecerimizlə əlaqə saxlayaraq, çatdırılma tarixini sizin üçün münasib olan istənilən digər vaxta keçirmək olar. 
                            Təcili sifarişlər və nümunə sifarişləri yalnız İçərişəhər metro staniyasının yaxınlığına çatdırıla bilər. Bu zaman nəzərə alın ki, sifarişi əvvəlcədən telefon vasitəsilə menecerimizlə razılaşdırmaq lazımdır, menecer təcili sifarişinizi qəbul etdikdən sonra çatdırılma ən azı 1 saat sonra həyata keçirilə bilər. </p>
                        </div>
                        <div class="mutual_delivery_single">
                          <div class="mutual_heading">
                            <p class="mutual_title">Xırdalan şəhəri üzrə:</p>                            
                          </div>
                          <p class="delivery_text">
                          Xırdalan şəhərinə çatdırılma sifariş edildikdən sonrakı gün ərzində əvvəlcədən razılaşdırılmış ünvana  həyata keçirilir.   
                          </p>
                        </div>
                        <div class="mutual_delivery_single">
                          <div class="mutual_heading">
                            <p class="mutual_title">Sumqayıt şəhəri üzrə:</p>                            
                          </div>
                          <p class="delivery_text">
                          Sumqayıt şəhərinə çatdırılma sifariş edildikdən sonrakı gün ərzində əvvəlcədən razılaşdırılmış vaxtda “Sumqayıt Sərnişin” vağzalına (Bakı-Sumqayıt sürətli qatarının  gəlişi stansiyası) həyata keçirilir.                             </p>
                        </div>
                        <div class="mutual_delivery_single">
                          <div class="mutual_heading">
                            <p class="mutual_title">Ölkə üzrə:</p>
                          </div>
                          <p class="delivery_text">
                          Azərbaycanın rayonlarına çatdırılma sifarişin tam dəyərinin əvvəlcədən ödənilməsi şərti ilə (ödəmə şərtləri ilə burada tanış ola bilərsiniz) 3-5 gün ərzində həyata keçirilir.                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
