<?php
    include("includes/head.php");
?>



<section class="project_inner">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
        <?php
            include("includes/category.list.php");
        ?>
        <div class="container">
          <div class="row">
              <div class="project_inner_container w-100">
                <div class="breadcrumbs">
                    <span class="old_page">Azclimart</span>
                    <img src="img/breadcrumb.svg" alt="">
                    <span class="old_page">Corella boyaları</span>
                    <img src="img/breadcrumb.svg" alt="">
                    <a href="#" class="new_page">Emusiyalar</a>
                </div>
                <div class="project_top_container">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="project_loop_box project_inner_img">
                                    <img src="img/loop_img.jpg" alt="" class="zoom_img">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="project_loop_box project_inner_img">
                                    <img src="img/block_images.png" alt="" class="zoom_img">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="one_img_box project_inner_img">
                        <img src="img/loop_img.jpg" alt="">
                    </div> -->
                    <div class="project_inner_detail">
                        <p class="project_name">Parket lakı</p>
                        <input type="hidden" value="" name="star">
                        <div class="project_inner_middle">
                            <div class="project_adding_info_rating">
                                <ul class="feedback_rating">
                                    <li class="rated" data-star="1">
                                        <svg width="15" height="14" viewBox="0 0 15 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M14.0759 4.86192L9.86075 4.24932L7.97647 0.429303C7.92501 0.324713 7.84034 0.240045 7.73575 0.18858C7.47345 0.0590882 7.1547 0.166998 7.02354 0.429303L5.13927 4.24932L0.924129 4.86192C0.807918 4.87852 0.701668 4.93331 0.620321 5.01631C0.521976 5.1174 0.467783 5.25339 0.469651 5.3944C0.471519 5.53542 0.529294 5.66992 0.630282 5.76837L3.67999 8.7417L2.95948 12.9402C2.94258 13.0379 2.95339 13.1384 2.99068 13.2302C3.02796 13.322 3.09024 13.4016 3.17043 13.4598C3.25063 13.5181 3.34555 13.5527 3.44441 13.5597C3.54328 13.5668 3.64214 13.546 3.72979 13.4997L7.50001 11.5175L11.2702 13.4997C11.3732 13.5545 11.4927 13.5728 11.6072 13.5528C11.8961 13.503 12.0903 13.2291 12.0405 12.9402L11.32 8.7417L14.3697 5.76837C14.4527 5.68702 14.5075 5.58077 14.5241 5.46456C14.569 5.17403 14.3664 4.90508 14.0759 4.86192Z" fill="#B0D2F5"/>
                                        </svg>

                                    </li>
                                    <li class="rated" data-star="2">
                                        <svg width="15" height="14" viewBox="0 0 15 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M14.0759 4.86192L9.86075 4.24932L7.97647 0.429303C7.92501 0.324713 7.84034 0.240045 7.73575 0.18858C7.47345 0.0590882 7.1547 0.166998 7.02354 0.429303L5.13927 4.24932L0.924129 4.86192C0.807918 4.87852 0.701668 4.93331 0.620321 5.01631C0.521976 5.1174 0.467783 5.25339 0.469651 5.3944C0.471519 5.53542 0.529294 5.66992 0.630282 5.76837L3.67999 8.7417L2.95948 12.9402C2.94258 13.0379 2.95339 13.1384 2.99068 13.2302C3.02796 13.322 3.09024 13.4016 3.17043 13.4598C3.25063 13.5181 3.34555 13.5527 3.44441 13.5597C3.54328 13.5668 3.64214 13.546 3.72979 13.4997L7.50001 11.5175L11.2702 13.4997C11.3732 13.5545 11.4927 13.5728 11.6072 13.5528C11.8961 13.503 12.0903 13.2291 12.0405 12.9402L11.32 8.7417L14.3697 5.76837C14.4527 5.68702 14.5075 5.58077 14.5241 5.46456C14.569 5.17403 14.3664 4.90508 14.0759 4.86192Z" fill="#B0D2F5"/>
                                        </svg>

                                    </li>
                                    <li class="rated" data-star="3">
                                        <svg width="15" height="14" viewBox="0 0 15 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M14.0759 4.86192L9.86075 4.24932L7.97647 0.429303C7.92501 0.324713 7.84034 0.240045 7.73575 0.18858C7.47345 0.0590882 7.1547 0.166998 7.02354 0.429303L5.13927 4.24932L0.924129 4.86192C0.807918 4.87852 0.701668 4.93331 0.620321 5.01631C0.521976 5.1174 0.467783 5.25339 0.469651 5.3944C0.471519 5.53542 0.529294 5.66992 0.630282 5.76837L3.67999 8.7417L2.95948 12.9402C2.94258 13.0379 2.95339 13.1384 2.99068 13.2302C3.02796 13.322 3.09024 13.4016 3.17043 13.4598C3.25063 13.5181 3.34555 13.5527 3.44441 13.5597C3.54328 13.5668 3.64214 13.546 3.72979 13.4997L7.50001 11.5175L11.2702 13.4997C11.3732 13.5545 11.4927 13.5728 11.6072 13.5528C11.8961 13.503 12.0903 13.2291 12.0405 12.9402L11.32 8.7417L14.3697 5.76837C14.4527 5.68702 14.5075 5.58077 14.5241 5.46456C14.569 5.17403 14.3664 4.90508 14.0759 4.86192Z" fill="#B0D2F5"/>
                                        </svg>

                                    </li>
                                    <li class="rated" data-star="4">
                                        <svg width="15" height="14" viewBox="0 0 15 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M14.0759 4.86192L9.86075 4.24932L7.97647 0.429303C7.92501 0.324713 7.84034 0.240045 7.73575 0.18858C7.47345 0.0590882 7.1547 0.166998 7.02354 0.429303L5.13927 4.24932L0.924129 4.86192C0.807918 4.87852 0.701668 4.93331 0.620321 5.01631C0.521976 5.1174 0.467783 5.25339 0.469651 5.3944C0.471519 5.53542 0.529294 5.66992 0.630282 5.76837L3.67999 8.7417L2.95948 12.9402C2.94258 13.0379 2.95339 13.1384 2.99068 13.2302C3.02796 13.322 3.09024 13.4016 3.17043 13.4598C3.25063 13.5181 3.34555 13.5527 3.44441 13.5597C3.54328 13.5668 3.64214 13.546 3.72979 13.4997L7.50001 11.5175L11.2702 13.4997C11.3732 13.5545 11.4927 13.5728 11.6072 13.5528C11.8961 13.503 12.0903 13.2291 12.0405 12.9402L11.32 8.7417L14.3697 5.76837C14.4527 5.68702 14.5075 5.58077 14.5241 5.46456C14.569 5.17403 14.3664 4.90508 14.0759 4.86192Z" fill="#B0D2F5"/>
                                        </svg>

                                    </li>
                                    <li class="rated" data-star="5">
                                        <svg width="15" height="14" viewBox="0 0 15 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M14.0759 4.86192L9.86075 4.24932L7.97647 0.429303C7.92501 0.324713 7.84034 0.240045 7.73575 0.18858C7.47345 0.0590882 7.1547 0.166998 7.02354 0.429303L5.13927 4.24932L0.924129 4.86192C0.807918 4.87852 0.701668 4.93331 0.620321 5.01631C0.521976 5.1174 0.467783 5.25339 0.469651 5.3944C0.471519 5.53542 0.529294 5.66992 0.630282 5.76837L3.67999 8.7417L2.95948 12.9402C2.94258 13.0379 2.95339 13.1384 2.99068 13.2302C3.02796 13.322 3.09024 13.4016 3.17043 13.4598C3.25063 13.5181 3.34555 13.5527 3.44441 13.5597C3.54328 13.5668 3.64214 13.546 3.72979 13.4997L7.50001 11.5175L11.2702 13.4997C11.3732 13.5545 11.4927 13.5728 11.6072 13.5528C11.8961 13.503 12.0903 13.2291 12.0405 12.9402L11.32 8.7417L14.3697 5.76837C14.4527 5.68702 14.5075 5.58077 14.5241 5.46456C14.569 5.17403 14.3664 4.90508 14.0759 4.86192Z" fill="#B0D2F5"/>
                                        </svg>

                                    </li>
                                </ul>
                                <div class="in_depo same_inner_box">
                                    <div class="custom_check exist"></div>
                                    <p class="info_desc_project">Depoda var</p>
                                </div>
                                <div class="same_inner_box">
                                    <div class="little_img"><img src="img/carbon_delivery.svg" alt=""></div>
                                    <p class="info_desc_project">Ödənişsiz çatdırılma</p>
                                </div>
                                <div class="same_inner_box">
                                    <div class="little_img"><img src="img/in_heart.svg" alt=""></div>
                                    <p class="info_desc_project">Sevilənlərə əlavə et</p>
                                </div>
                            </div>
                            <div class="project_inner_numbers">
                                <div class="project_inner_single">
                                    <p>Məhsul kodu :</p>
                                    <p>001</p>
                                </div>
                                <div class="project_inner_single">
                                    <p>Məhsul :</p>
                                    <p>00035</p>
                                </div>
                            </div>
                            <div class="project_size_box">
                                <p>Məhsul ölçüsü</p>
                                <div class="size_grid">
                                    <button class="size_one">15kq</button>
                                    <button class="size_one">5kq</button>
                                    <button class="size_one">10kq</button>
                                    <button class="size_one">15kq</button>
                                    <button class="size_one">25kq</button>
                                    <button class="size_one">35kq</button>
                                    <button class="size_one">45kq</button>
                                    <button class="size_one">55kq</button>
                                </div>
                            </div>
                            <div class="project_color_box">
                                <select name="color" class="nice-select">
                                    <option value= "" disabled selected> Rəng seçin</option>
                                    <option value="1">LGT 4662</option>
                                    <option value="2">LGT 4662</option>
                                    <option value="3">LGT 4662</option>
                                    <option value="4">LGT 4662</option>
                                    <option value="5">LGT 4662</option>
                                    <option value="6">LGT 4662</option>
                                    <option value="7">LGT 4662</option>
                                    <option value="8">LGT 4662</option>
                                </select>
                            </div>
                            <div class="project_inner_operation">
                                <div class="inner_count_box" data-target="inner_price">
                                    <button class="minus">-</button>
                                    <input type="number" id="inner_price" value="1" name="" min="1">
                                    <button class="plus">+</button>
                                </div>
                                <p><span class="price" data-price="75.8">75.8</span>₼</p>
                            </div>
                        </div>
                        <div class="project_inner_btns">
                            <button class="white-blue paint_basket">Səbətə əlavə et</button>
                            <a href="#" class="btn_blue">Bir başa al</a>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
                <div class="project_inner_tab_container">
                  <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                      <a 
                        class="nav-item nav-link active" 
                        id="info-tab" 
                        data-toggle="tab" 
                        href="#info" 
                        role="tab" 
                        aria-controls="info" 
                        aria-selected="true">
                        Tənzimləyici məlumatlar
                      </a>
                      <a 
                        class="nav-item nav-link" 
                        id="tech-tab" 
                        data-toggle="tab" 
                        href="#tech_indicators" 
                        role="tab" 
                        aria-controls="tech" 
                        aria-selected="false">
                        Texniki göstəricilər
                      </a>
                      <a 
                        class="nav-item nav-link" 
                        id="usage-tab" 
                        data-toggle="tab" 
                        href="#usage" 
                        role="tab" 
                        aria-controls="usage" 
                        aria-selected="false">
                        Tətbiq sahəsi
                      </a>
                    </div>
                  </nav>

                  <div class="tab-content" id="nav-tabContent" data-simplebar>
                    <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info-tab">
                      <p>
                        Yüksək keyfiyyətli nitro sellüloz və özəl alkid əsaslı, bir komponentli, tez quruyan, yüksək dolduruculuq xüsusiyyətinə malik, üzərinə vurulacaq son qat boya üçün düzgün və möhkəm səth yaradan astar boyadır. Alışma temperaturu : 21ºC
                        Özlülüyü, KU, 20ºC: 100-110
                        Məhsulun sıxlığı: 1.50±0.2 kq/litr ( rənglə bağlı olaraq)
                        UAB(uçucu üzvi birləşmələr) : ortalama 406 q/litr (UK-PG6/23(92)
                        <b>Tətbiq ediləcək səthin hazırlanması:</b>
                        a) Taxta səthin hazırlanması:
                        Corella Sellülozik Astar Boya tətbiq edilməzdən əvvəl yeni səth tozdan , yağ qalıqlarından və s.dən tam təmizlənib quru olmalıdır. Metal səthlərin üzərindən astar qat kimi istifadə edilir. Taxta üzərinə Sellülozik dolğu vernik vurulmalıdır. Əgər tətbiq olunacaq səthdə qüsurlar çoxdursa , düyün və qırıntılar varsa səth lazımı vasitə ilə hamarlanır.
                        a) Taxta səthin hazırlanması:
                        Corella Sellülozik Astar Boya tətbiq edilməzdən əvvəl yeni səth tozdan , yağ qalıqlarından və s.dən tam təmizlənib quru olmalıdır. Metal səthlərin üzərindən astar qat kimi istifadə edilir. Taxta üzərinə Sellülozik dolğu vernik vurulmalıdır. Əgər tətbiq olunacaq səthdə qüsurlar çoxdursa , düyün və qırıntılar varsa səth lazımı vasitə ilə hamarlanır.
                        a) Taxta səthin hazırlanması:
                        Corella Sellülozik Astar Boya tətbiq edilməzdən əvvəl yeni səth tozdan , yağ qalıqlarından və s.dən tam təmizlənib quru olmalıdır. Metal səthlərin üzərindən astar qat kimi istifadə edilir. Taxta üzərinə Sellülozik dolğu vernik vurulmalıdır. Əgər tətbiq olunacaq səthdə qüsurlar çoxdursa , düyün və qırıntılar varsa səth lazımı vasitə ilə hamarlanır.
                        a) Taxta səthin hazırlanması:
                        Corella Sellülozik Astar Boya tətbiq edilməzdən əvvəl yeni səth tozdan , yağ qalıqlarından və s.dən tam təmizlənib quru olmalıdır. Metal səthlərin üzərindən astar qat kimi istifadə edilir. Taxta üzərinə Sellülozik dolğu vernik vurulmalıdır. Əgər tətbiq olunacaq səthdə qüsurlar çoxdursa , düyün və qırıntılar varsa səth lazımı vasitə ilə hamarlanır.
                        a) Taxta səthin hazırlanması:
                        Corella Sellülozik Astar Boya tətbiq edilməzdən əvvəl yeni səth tozdan , yağ qalıqlarından və s.dən tam təmizlənib quru olmalıdır. Metal səthlərin üzərindən astar qat kimi istifadə edilir. Taxta üzərinə Sellülozik dolğu vernik vurulmalıdır. Əgər tətbiq olunacaq səthdə qüsurlar çoxdursa , düyün və qırıntılar varsa səth lazımı vasitə ilə hamarlanır.
                      </p>
                    </div>
                    <div class="tab-pane fade" id="tech_indicators" role="tabpanel" aria-labelledby="tech-tab">
                        <p>
                             Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
                        </p>
                    </div>
                    <div class="tab-pane fade" id="usage" role="tabpanel" aria-labelledby="usage-tab">
                        <p>
                          Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
                        </p>
                    </div>
                  </div>
                </div>

                <div class="project_inner_table_info">
                  <div class="project_inner_heading">
                    <p>Göstəriciləri</p>
                    <p>Əmsallar</p>
                  </div>
                  <div class="project_inner_body">
                    <div class="project_body_row">
                      <p>İlkin qaynama dərəcəsi (İQD)  (distillə olunmuş  98% min.), (°C)	</p>
                      <p>Parlaq, şəffaf maye</p>
                    </div>
                    <div class="project_body_row">
                      <p>İlkin qaynama dərəcəsi (İQD)  (distillə olunmuş  98% min.), (°C)	</p>
                      <p>Parlaq, şəffaf maye</p>
                    </div>
                    <div class="project_body_row">
                      <p>İlkin qaynama dərəcəsi (İQD)  (distillə olunmuş  98% min.), (°C)	</p>
                      <p>Parlaq, şəffaf maye</p>
                    </div>
                    <div class="project_body_row">
                      <p>İlkin qaynama dərəcəsi (İQD)  (distillə olunmuş  98% min.), (°C)	</p>
                      <p>Parlaq, şəffaf maye</p>
                    </div>
                  </div>
                </div>

                <div class="same_bottom_container same_swiper position-relative">
                    <div class="heading_container_same">
                        <p class="title_same_heading">Fərqli ehtiyac və istifadəyə uyğun funksional məhsullar</p>
                        <a href="#" class="see_all">Bütün məhsullara bax</a>
                    </div>
                    <div class="swiper-container same_grid_boxes">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
          </div>
        </div>
    </div>
    <div class="modal fade" id="imgShow" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button class="delete" data-dismiss="modal" aria-label="Close"><img src="img/esc.svg" alt=""></button>
                <div class="modal-img"><img src="" alt=""></div>
            </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
