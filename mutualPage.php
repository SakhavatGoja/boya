<?php
    include("includes/head.php");
?>


<section class="mutualPage">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
        <?php
            include("includes/category.list.php");
        ?>
        <div class="container">
            <div class="row">
                <div class="repeat_main_container">
                    <div class="breadcrumbs">
                        <span class="old_page">Azclimart</span>
                        <img src="img/breadcrumb.svg" alt="">
                        <a href="#" class="new_page">Yeni məhsullar</a>
                    </div>
                    <div class="repeat_filter_box">
                        <div class="heading_container_same">
                            <p class="title_same_heading">Yeni məhsullar</p>
                            <button class="filter"><img src="img/filter.svg" alt=""></button>
                        </div>
                        <div class="filter_grid">
                            <div class="paint_main_grid">
                                <div class="paints_grid">
                                    <div class="paint_box">
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                </div>
                                <button class="see_more">Daha ətraflı</button>
                            </div>
                            <aside class="aside-form">
                                <button class="esc"><img src="img/esc.svg" alt=""></button>
                                <form action="" class="left_filter_box">
                                    <div class="filter_header">
                                        <p class="filter_title">Filter</p>
                                        <button id="clean">Təmizlə</button>
                                    </div>
                                    <div class="filter_operation">
                                        <div class="filter_special_box">
                                            <div class="f_spe_heading">
                                                <p>Qiymət</p>
                                            </div>
                                            <div class="block range-block">
                                                <div class="range-line"></div>
                                                <input type="text" class="js-range-slider" name="my_range" value="" />
                                            </div>     
                                        </div>
                                        <div class="filter_special_box">
                                            <div class="f_spe_heading">
                                                <p>Səthlər</p>
                                            </div>
                                            <div class="block">
                                                <div class="label_box">
                                                    <div class="label_single">
                                                        <input type="checkbox" name="ceiling" id="ceiling">
                                                        <label for="ceiling" class="label_project">Tavanlar</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="wardrobe" id="wardrobe">
                                                        <label for="wardrobe" class="label_project">Şkaflar</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="door" id="door">
                                                        <label for="door" class="label_project">Qapılar</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="furniture" id="furniture">
                                                        <label for="furniture" class="label_project">Mebel</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="metal" id="metal">
                                                        <label for="metal" class="label_project">Metal</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="fayan" id="fayan">
                                                        <label for="fayan" class="label_project">Fayans</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="wall" id="wall">
                                                        <label for="wall" class="label_project">Divarlar</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="window" id="window">
                                                        <label for="window" class="label_project">Pəncərələr</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="beton" id="beton">
                                                        <label for="beton" class="label_project">Beton</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="filter_special_box">
                                            <div class="f_spe_heading">
                                                <p>Tətbiq növü</p>
                                            </div>
                                            <div class="block">
                                                <div class="label_box">
                                                    <div class="label_single">
                                                        <input type="checkbox" name="interior" id="interior">
                                                        <label for="interior" class="label_project">İnteryer</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="exterior" id="exterior">
                                                        <label for="exterior" class="label_project">Eksteriyer</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </aside>
                        </div>
                    </div>
                    <div class="same_bottom_container same_swiper">
                        <div class="heading_container_same">
                            <p class="title_same_heading">Fərqli ehtiyac və istifadəyə uyğun funksional məhsullar</p>
                            <a href="#" class="see_all">Bütün məhsullara bax</a>
                        </div>
                        <div class="swiper-container same_grid_boxes">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="paint_box">
                                        <div class="discount">
                                            <p>45%</p>
                                        </div>
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="paint_box">
                                        <div class="discount">
                                            <p>45%</p>
                                        </div>
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="paint_box">
                                        <div class="discount">
                                            <p>45%</p>
                                        </div>
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="paint_box">
                                        <div class="discount">
                                            <p>45%</p>
                                        </div>
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="paint_box">
                                        <div class="discount">
                                            <p>45%</p>
                                        </div>
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="paint_box">
                                        <div class="discount">
                                            <p>45%</p>
                                        </div>
                                        <button class="heart">
                                            <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                            </svg>
                                        </button>
                                        <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                        <div class="paint_desc">
                                            <p class="paint_firm">Corella boyaları</p>
                                            <p class="paint_name">Emulsiyalar</p>
                                            <p class="paint_type">Super Plus</p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
