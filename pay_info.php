<?php
    include("includes/head.php");
?>


<section class="delivery pay_info">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
        <?php
            include("includes/category.list.php");
        ?>
        <div class="container">
          <div class="row">
              <div class="delivery_main_container">
                  <div class="breadcrumbs">
                      <span class="old_page">Azclimart</span>
                      <img src="img/breadcrumb.svg" alt="">
                      <a href="#" class="new_page">Məkana özəl məhsullarımız</a>
                  </div>
                  <div class="heading_container_same">
                      <p class="title_same_heading">Ödəmə</p>
                  </div>
                  <div class="delivery_info_container">
                    <p class="delivery_desc">
                      Bizdən sifariş edərkən, Siz həm nəğd, həm də bank kartları vasitəsilə ödəniş edə bilərsiniz. 
                      Sifariş ödənişlərinizin rahat həyata keçirilməsi üçün təqdim etdiyimiz istənilən ödəniş üsulundan yararlana bilərsiniz.
                    </p>
                    <p class="same_blue_title">Bizdə mümkün olan ödəniş üsulları:</p>
                    <div class="mutual_delivery_info_box">
                      <div class="mutual_delivery_single">
                        <div class="mutual_heading">
                          <div class="left_heading_container">
                            <p class="mutual_title">Onlayn ödəniş:</p>                            
                            <p class="heading_desc">Biz bu kredit və debet kartları ilə ödənişi qəbul edirik:</p>
                          </div>
                          <div class="payment_tools">
                            <div><img src="img/master.png" alt=""></div>
                            <div><img src="img/source.png" alt=""></div>
                            <div><img src="img/visa.png" alt=""></div>
                            <div><img src="img/verified.png" alt=""></div>
                          </div>                           
                        </div>
                        <p class="delivery_text">
                            Bank kartı İnternetdə ödəniş üçün yararlı olmalıdır (bu, bank tərəfindən müəyyən olunur);
                            Sifarişin bank kartı vasitəsilə onlayn ödənişi zamanı, ödəmə prosesi elektron ödənişlər sisteminin saytında həyata keçirilir. Qeyd etdiyiniz məlumatlar – kartın rekvizitləri, qeydiyyat məlumatları bizim onlayn mağazamızın işçiləri tərəfindən baxıla bilmir.  Bütün məxfi məlumatlar müştəridən ödəniş sistemininə və bankın serverinə ötürülür.  
                            Saytımızda onlayn ödənişi seçərək, Siz sifarişiniz üçün ən sürətli və təhlükəsiz ödəniş üsulunu seçmiş olursunuz.
                              Onlayn ödəniş zamanı sizin kartlarınız və pul vəsaitiniz barədə məlumat bizə daxil olmur, ödəniş bank və ödəniş 
                              sistemləri vasitəsilə həyata keçirilir. Onlayn ödəniş üsulunu seçdikdən və sifarişi təsdiq etdikdən sonra Siz 
                              avtomatik olaraq əməkdaşlıq etdiyimiz bankın və ödəniş sisteminin mühafizə olunan səhifəsinə yönəldilirsiniz. 
                              Sizin ödənişiniz onlayn ödənişin beynəlxalq təhlükəsizlik standartlarına uyğun olaraq həyata keçiriləcək.  
                        </p>
                      </div>
                      <div class="mutual_delivery_single">
                        <div class="mutual_heading">
                          <p class="mutual_title">Çatdırılma zamanı nəğd ödəniş :</p>                            
                        </div>
                        <p class="delivery_head_text">Siz sifarişin dəyərini çatdırılma zamanı nəğd şəklində əməkdaşımıza ödəyə bilərsiniz:</p>
                        <p class="delivery_text">
                        Ödəniş zamanı Yalnız Azərbaycan Respublikasının milli valyutası – AZN ödənişə qəbul olunur.
                        </p>
                      </div>
                      <div class="mutual_delivery_single">
                        <div class="mutual_heading">
                          <p class="mutual_title">Bank və poçt köçürmələri ilə ödəniş:</p>                            
                        </div>
                        <p class="delivery_text">
                          Bu ödəniş üsulu Azərbaycan bölgələrinə çatdırılma ilə sifariş etdiyiniz və onlayn ödəyə bilmədiyiniz zaman tətbiq olunur. 
                          Nəzərə almağınızı xahiş edirik ki, sifariş Azərbaycanın bölgələrina çatdırılan zamanı sifarişin dəyəri 100% əvvəlcədən ödənilməlidir.
                          Çatdırılma haqqında daha ətraflı  məlumatı buradan əldə edə bilərsiniz.
                        </p>
                      </div>
                    </div>
                    <div class="pay_info_condition">
                      <span>*</span>
                      <p>Yarana biləcək bütün əlavə suallarınızla bağlı bizimlə əlaqə saxlamağınız xahiş olunur.  </p>
                    </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
