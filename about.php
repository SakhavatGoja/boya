<?php
    include("includes/head.php");
?>


<section class="about_blog_inner">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
        <?php
            include("includes/category.list.php");
        ?>
        <div class="container">
            <div class="row">
                <div class="blog_main_container">
                    <div class="breadcrumbs">
                        <span class="old_page">Azclimart</span>
                        <img src="img/breadcrumb.svg" alt="">
                        <a href="#" class="new_page">Məkana özəl məhsullarımız</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="img-container-1600">
          <img src="img/testabout.jpg" alt="">
        </div>
        <div class="container">
          <div class="row">
            <div class="heading_container_same">
                <p class="title_same_heading">Haqqımızda</p>
            </div>
            <div class="about_text_container">
              <h1 class="about_title">Rənglər həyatımızın ayrılmaz hissəsidir.</h1>
              <div class="column_2">
                 Biz adətən gördüyümüz əşyaları, məkanı rənglər vasitəsilə qəbul edirik.
                 Həmin rənglərdən asılı olaraq reaksiyamız da ona uyğun olaraq dəyişə bilir. “Rənglər və zövqlər müqaisə olunmur” deyirlər.
                 Bu baxımdan kimin hansı rəngə üstünlük verməsi həmin şəxsin öz daxili hisslərinə, psixoloji durumuna əsaslanır.
                 Lakin ümumiləşdirmə aparsaq aşağıdakı nəticələrə gəlmək olar. Bizə məlum olan bütün rənglər 3 qrupa ayrılır:
                 Birinci qrup: Qırmızı, mavi və sarı. Bu rəngləri əldə etmək üçün digər rənglərin qarışımı işimizə yaramır.
                 Bu rənglər heç bir rəngin qarışımından alına bilmir. İkinci qrup rənglər: Narıncı, bənövşəyi və yaşıl.
                 Bu rənglər bərabər miqdarda qatılan iki ana rəngin qarışımından əldə edilə bilər. Qırmızı ilə sarının
                 birləşməsindən narıncı; mavi ilə qırmızının birləşməsindən bənövşəyi, mavi ilə sarının birləşməsindən 
                 yaşıl əldə edilir. Üçüncü qrup rənglər: Bu rənglər birinci və ikinci qrup rənglərin fərqli nisbətlərdə
                 qarışımından əldə edilən rənglərdir Bəzi məkanlar üçün tövsiyə edilən rənglər vardır. Məsələn, boz rəng 
                 mətbəx və yemək yeyilən yerlər üçün uyğun rəng hesab olunur. Mavi və yaşıl rəng yataq otaqları üçün uyğundur. 
                 Rənglər öz tamamlayıcı rəngləri ilə çox yaxşı tandem yaradırlar. Məsələn, mavinin tamamlayıcı rəngləri sarı,
                 yaşıl və ağ rəngdir. 
                 <p>Rənglərin insanın psixoloji vəziyyətləri üzərində təsirləri də vardır</p>
                 Məsələn ağ rəng 
                 insanda yetərlilik hissi yaradır, qırmızı rəng xəbərdaredici, sarı rəng məkanı işıqlandırır və istilik bəxş edir,
                 yaşıl rəngin tonları ətrafa təbiət abu havası və təmizlik hissi bəxş etməkdə yardımçı olur, qəhvəyi rəng də torpağı
                 xatırladır, mavi dənizlərin və səmanın rəngidir insana bir sərinlik bəxş edir.
                 Boyaların insan üzərində olan təsirləri fiziki və psixoloji olaraq iki yerə ayrılır. Fiziki olaraq məkanı böyük və ya kiçik göstərmək,
                 məkanı olduğundan isti və ya sərin göstərmək, divari irəlilətmək, tavanı alçaltmaq kimi təsirlərini göstərmək olar. Estetik olaraq isə;
                 rahatlıq verən, xəbərdar edən, sevinc və ya kədər hissi bəxş edən, modern və klassik kimi modellərdə cəmləşdirmək olar. Qırmızı, narıncı
                 və sarı bir istilik hissi yaradırlar və gördüyünüz zaman sizə doğru gəlirlərmiş kimi effekt verirlər. Mavi, yaşıl və bənövşəyi rənglər
                 sərin rənglərdir. Gördüyünüz zaman sizdən uzaqlaşırmış kimi hiss bəxş edirlər. Açıq rəngli boyalar daha çox işıq əks elətdirir və
                 sizə daha çox pozitiv hisslər yaşadır. Açıq rəngli boyalar məkanı daha geniş göstərirlər. Tünd rəngli boyalar isə işiğı əmir və
                 əşyaları daha yaxın göstərirlər və əgər geniş məkanlarda istifadə edilərlərsə insan üzərində bir sıxıntı hissi qoyurlar. Tünd
                 rənglər məkanı olduğundan daha kiçik göstərirlər. Parlaq rənglər də əşyaları olduğundan böyük göstərirlər. Boyanın rəngini seçərkən
                 məkanın şəkili, böyüklüyü, duruşu və aydınlıq olub-olmaması diqqət edilməli məqamlardır, Rənglər memarlıq vəziyyətini dəyişmək 
                 üçün də uğurla istifadə oluna bilir. Məsələn, bir məkanın küncləri tünd rəngə boyanaraq həmin məkan kub şəklində göstərilə bilər
                 və ya yüksək bir tavan tünd rəngə boyanarsa, alçaqmış kimi görünüş verə bilər. Parlaq işığa məruz qalan hissələr tünd rəngə 
                 boyanmalı; işıq almayan məkanlar isə açıq rəngə boyanarsa daha uyğun olar.
                 Yemək otaqları iştahı öldürməmək xatirinə isti rəngə boyanılarsa daha uğurlu olar.
              </div>
            </div>
          </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
