<?php
    include("includes/head.php");
?>


<section class="contact">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
      <?php
          include("includes/category.list.php");
      ?>
      <div class="container">
        <div class="row">
            <div class="contact_main_container">
                <div class="breadcrumbs">
                    <span class="old_page">Azclimart</span>
                    <img src="img/breadcrumb.svg" alt="">
                    <a href="#" class="new_page">Əlaqə</a>
                </div>
                <div class="contact_grid_box w-100">
                  <div class="contact_left">
                    <div class="heading_container_same">
                        <p class="title_same_heading">Əlaqə</p>
                    </div>
                    <div class="contact_call contact_same">
                      <div class="left_icon"><img src="img/call_center.png" alt=""></div>
                      <div class="phones">
                        <p class="same_contact_text">Çağrı mərkəzi *5111</p>
                      </div>
                    </div>
                    <div class="contact_phone contact_same">
                      <div class="left_icon"><img src="img/phone.svg" alt=""></div>
                      <div class="phones">
                        <p class="same_contact_text">+99451 206 53 66</p>
                        <p class="same_contact_text">+99451 206 53 66</p>
                      </div>
                    </div>
                    <div class="contact_address contact_same">
                      <div class="left_icon"><img src="img/location.png" alt=""></div>
                      <div class="phones">
                        <p class="same_contact_text">Bakı ş. Nərimanov ray. Ziya</p>
                      </div>
                    </div>
                    <div class="contact_mail contact_same">
                      <div class="left_icon"><img src="img/mail.svg" alt=""></div>
                      <div class="phones">
                        <p class="same_contact_text">info@boya.az</p>
                      </div>
                    </div>
                  </div>
                  <div class="contact_right">
                    <p class="write_us">Bizə yazın</p>
                    <form action="" id="contact_form">
                      <div class="form-group">
                        <input class="inputValidate" type="text" name="name" required>
                        <label class="place-label">Ad <span>*</span></label>
                      </div>
                      <div class="form-group">
                        <input class="inputValidate" type="text" name="father_name" required>
                        <label class="place-label">Soyad  Ata adı <span>*</span></label>
                      </div>
                      <div class="form-group">
                        <input class="inputValidate" type="email" name="email" required>
                        <label class="place-label">Email <span>*</span></label>
                      </div>
                      <div class="form-group">
                        <input class="inputValidate number_input" type="number" minlength="10" name="phone" required>
                        <label class="place-label">Telefon nömrəsi <span>*</span></label>
                      </div>
                      <div class="form-group textarea_group">
                        <textarea name="textarea"></textarea>
                        <label class="place-label">Əlavə qeyd </label>
                      </div>
                      <button type="submit" class="btn_blue">Göndər</button>
                    </form>
                  </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
