<?php
    include("includes/head.php");
?>


<section class="notFound">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
        <?php
            include("includes/category.list.php");
        ?>
        <div class="container">
            <div class="row">
                <div class="404_main_container w-100">
                    <div class="breadcrumbs">
                        <a href="index.php" class="new_page">Ana səhifəyə qayıt</a>
                        <img src="img/breadcrumb.svg" alt="">
                    </div>
                    <div class="inner_404 w-100">
                      <div class="notFound_img">
                        <img src="img/404.png" alt="">
                      </div>
                      <p class="not_found_text">
                        Səhifə tapılmadı
                      </p>
                      <div class="brush">
                        <img src="img/brush.png" alt="">
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
