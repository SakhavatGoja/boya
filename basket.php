<?php
    include("includes/head.php");
?>


<section class="basket">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
        <?php
            include("includes/category.list.php");
        ?>
        <div class="container">
          <div class="row">
              <div class="basket_main_container w-100">
                <div class="breadcrumbs">
                    <span class="old_page">Azclimart</span>
                    <img src="img/breadcrumb.svg" alt="">
                    <a href="#" class="new_page">Məkana özəl məhsullarımız</a>
                </div>
                <div class="heading_container_same">
                    <p class="title_same_heading">Səbət</p>
                    <p class="basket_count">Məhsul sayı - <span id="count_page"></span></p>
                </div>
                <div class="basket_grid">
                  <input type="hidden" name="" class="delivery_price_input" value="10">
                  <div class="basket_total_boxes">
                    <div class="basket_single_box">
                      <div class="basket_left">
                        <div class="basket_product_img"><img src="img/basket_paint.jpg" alt=""></div>
                        <div class="basket_product_info">
                          <div class="basket_product_name">
                            <p>Parket tikurella lakı</p>
                          </div>
                          
                          <p class="basket_product_code">Məhsul kodu :<span>001</span></p>
                        </div>
                        <div class="basket_product_operation">
                          <div class="basket_count_box" data-target="amount-1">
                            <button class="cart-minus-1"><img src="img/minus.svg" alt=""></button>
                            <input type="number" id="amount-1" value="1" name="" min="1">
                            <button class="cart-plus-1"><img src="img/plus.svg" alt=""></button>
                          </div>
                          <p class="money_price" ><span data-price="75.80">75.80</span>₼</p>
                        </div>
                      </div>
                      <button class="delete"><img src="img/esc.svg" alt=""></button>
                    </div>
                    <div class="basket_single_box">
                      <div class="basket_left">
                        <div class="basket_product_img"><img src="img/basket_paint.jpg" alt=""></div>
                        <div class="basket_product_info">
                          <div class="basket_product_name">
                            <p>Parket tikurella lakı</p>
                          </div>
                          
                          <p class="basket_product_code">Məhsul kodu :<span>001</span></p>
                        </div>
                        <div class="basket_product_operation">
                          <div class="basket_count_box" data-target="amount-2">
                            <button class="cart-minus-1"><img src="img/minus.svg" alt=""></button>
                            <input type="number" id="amount-2" value="1" name="" min="1">
                            <button class="cart-plus-1"><img src="img/plus.svg" alt=""></button>
                          </div>
                          <p class="money_price" ><span data-price="75.80">75.80</span>₼</p>
                        </div>
                      </div>
                      <button class="delete"><img src="img/esc.svg" alt=""></button>
                    </div>
                    <div class="basket_single_box">
                      <div class="basket_left">
                        <div class="basket_product_img"><img src="img/basket_paint.jpg" alt=""></div>
                        <div class="basket_product_info">
                          <div class="basket_product_name">
                            <p>Parket tikurella lakı</p>
                          </div>
                          
                          <p class="basket_product_code">Məhsul kodu :<span>001</span></p>
                        </div>
                        <div class="basket_product_operation">
                          <div class="basket_count_box" data-target="amount-3">
                            <button class="cart-minus-1"><img src="img/minus.svg" alt=""></button>
                            <input type="number" id="amount-3" value="1" name="" min="1">
                            <button class="cart-plus-1"><img src="img/plus.svg" alt=""></button>
                          </div>
                          <p class="money_price" ><span data-price="75.80">75.80</span>₼</p>
                        </div>
                      </div>
                      <button class="delete"><img src="img/esc.svg" alt=""></button>
                    </div>
                    <div class="basket_single_box">
                      <div class="basket_left">
                        <div class="basket_product_img"><img src="img/basket_paint.jpg" alt=""></div>
                        <div class="basket_product_info">
                          <div class="basket_product_name">
                            <p>Parket tikurella lakı</p>
                          </div>
                          
                          <p class="basket_product_code">Məhsul kodu :<span>001</span></p>
                        </div>
                        <div class="basket_product_operation">
                          <div class="basket_count_box" data-target="amount-4">
                            <button class="cart-minus-1"><img src="img/minus.svg" alt=""></button>
                            <input type="number" id="amount-4" value="1" name="" min="1">
                            <button class="cart-plus-1"><img src="img/plus.svg" alt=""></button>
                          </div>
                          <p class="money_price" ><span data-price="75.80">75.80</span>₼</p>
                        </div>
                      </div>
                      <button class="delete"><img src="img/esc.svg" alt=""></button>
                    </div>
                    <div class="basket_single_box">
                      <div class="basket_left">
                        <div class="basket_product_img"><img src="img/basket_paint.jpg" alt=""></div>
                        <div class="basket_product_info">
                          <div class="basket_product_name">
                            <p>Parket tikurella lakı</p>
                          </div>
                          
                          <p class="basket_product_code">Məhsul kodu :<span>001</span></p>
                        </div>
                        <div class="basket_product_operation">
                          <div class="basket_count_box" data-target="amount-5">
                            <button class="cart-minus-1"><img src="img/minus.svg" alt=""></button>
                            <input type="number" id="amount-5" value="1" name="" min="1">
                            <button class="cart-plus-1"><img src="img/plus.svg" alt=""></button>
                          </div>
                          <p class="money_price" ><span data-price="75.80">75.80</span>₼</p>
                        </div>
                      </div>
                      <button class="delete"><img src="img/esc.svg" alt=""></button>
                    </div>
                    <div class="basket_single_box">
                      <div class="basket_left">
                        <div class="basket_product_img"><img src="img/basket_paint.jpg" alt=""></div>
                        <div class="basket_product_info">
                          <div class="basket_product_name">
                            <p>Parket tikurella lakı</p>
                          </div>
                          
                          <p class="basket_product_code">Məhsul kodu :<span>001</span></p>
                        </div>
                        <div class="basket_product_operation">
                          <div class="basket_count_box" data-target="amount-6">
                            <button class="cart-minus-1"><img src="img/minus.svg" alt=""></button>
                            <input type="number" id="amount-6" value="1" name="" min="1">
                            <button class="cart-plus-1"><img src="img/plus.svg" alt=""></button>
                          </div>
                          <p class="money_price" ><span data-price="75.80">75.80</span>₼</p>
                        </div>
                      </div>
                      <button class="delete"><img src="img/esc.svg" alt=""></button>
                    </div>
                  </div>
                  <div class="result_box">
                    <div class="title_total_box">
                      <div class="heading">Yekun ödəniləcək qiymət</div>
                      <p>
                        <span id="total"></span>₼
                      </p>
                      <div class="delivery_box">
                        <img src="img/carbon_delivery.svg" alt="">
                        <p class="content">Çatdırılma haqqı</p>
                        <div class="line_delivery"></div>
                        <p class="delivery_price">
                          <span>25</span>
                          ₼
                        </p>
                      </div>
                    </div>
                    <a href="pay.php" class="btn_blue">Sifarişi rəsmiləşdir</a>
                  </div>
                </div>
              </div>
              
          </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
