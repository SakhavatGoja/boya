<div class="repeat_section_links">
  <div class="container">
      <div class="row">
          <div class="hovered_links">
            <div class="link_parent">
              <a href="#" class="main_link">Corella boyaları</a>
              <div class="link_child">
                <a href="#">Emulsiyalar</a>
                <a href="#">Sintetik boyalar</a>
                <a href="#">Sənaye boyaları</a>
                <a href="#">Laklar dolğular</a>
                <a href="#">Yapışkanlar</a>
                <a href="#">Tinerlər</a>
              </div>
            </div>
            <div class="link_parent">
              <a href="#" class="main_link">Azpol toz məhsulları</a>
              <div class="link_child">
                <a href="#">Emulsiyalar</a>
                <a href="#">Sintetik boyalar</a>
                <a href="#">Sənaye boyaları</a>
                <a href="#">Laklar dolğular</a>
                <a href="#">Yapışkanlar</a>
                <a href="#">Tinerlər</a>
              </div>
            </div>
            <div class="link_parent">
              <a href="#" class="main_link">Azpol sənaye məhsulları</a>
              <div class="link_child">
                <a href="#">Emulsiyalar</a>
                <a href="#">Sintetik boyalar</a>
                <a href="#">Sənaye boyaları</a>
                <a href="#">Laklar dolğular</a>
                <a href="#">Yapışkanlar</a>
                <a href="#">Tinerlər</a>
              </div>
            </div>
            <div class="link_parent">
              <a href="#" class="main_link">Azpol mebel məhsulları</a>
              <div class="link_child">
                <a href="#">Emulsiyalar</a>
                <a href="#">Sintetik boyalar</a>
                <a href="#">Sənaye boyaları</a>
                <a href="#">Laklar dolğular</a>
                <a href="#">Yapışkanlar</a>
                <a href="#">Tinerlər</a>
              </div>
            </div>
            <div class="link_parent">
              <a href="#" class="main_link">Muflon boyaları</a>
              <div class="link_child">
                <div class="row_child">
                  <a href="#">
                    Emulsiyalar
                  </a>
                  <div class="submenu_inner">
                    <a href="#">Mat</a>
                    <a href="#">Parlaq</a>
                  </div>
                </div>
                <div class="row_child">
                  <a href="#">
                    Emulsiyalar
                  </a>
                  <div class="submenu_inner">
                    <a href="#">Mat</a>
                    <a href="#">Parlaq</a>
                  </div>
                </div>
                <div class="row_child">
                  <a href="#">
                    Emulsiyalar
                  </a>
                  <div class="submenu_inner">
                    <a href="#">Mat</a>
                    <a href="#">Parlaq</a>
                  </div>
                </div>
                <div class="row_child">
                  <a href="#">
                    Emulsiyalar
                  </a>
                  <div class="submenu_inner">
                    <a href="#">Mat</a>
                    <a href="#">Parlaq</a>
                  </div>
                </div>
                <div class="row_child">
                  <a href="#">
                    Emulsiyalar
                  </a>
                  <div class="submenu_inner">
                    <a href="#">Mat</a>
                    <a href="#">Parlaq</a>
                  </div>
                </div>
                <div class="row_child">
                  <a href="#">
                    Emulsiyalar
                  </a>
                  <div class="submenu_inner">
                    <a href="#">Mat</a>
                    <a href="#">Parlaq</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="link_parent">
              <a href="#" class="main_link">Bermuda boyaları</a>
              <div class="link_child">
                <a href="#">Emulsiyalar</a>
                <a href="#">Sintetik boyalar</a>
                <a href="#">Sənaye boyaları</a>
                <a href="#">Laklar dolğular</a>
                <a href="#">Yapışkanlar</a>
                <a href="#">Tinerlər</a>
              </div>
            </div>
            <div class="link_parent">
              <a href="#" class="main_link">Ral kataloqları</a>
            </div>
          </div>
      </div>
  </div>
</div>