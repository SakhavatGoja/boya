<header>
  <div class="header_main_container">

    <div class="header_top_container">
      <div class="container">
        <div class="row">
          <div class="header_top_flex">
            <a href="delivery.php">Çatdırılma</a>
            <a href="fag.php">Tez-tez verilən suallar</a>
            <a href="#">Çağrı mərkəzi <span>*5111</span></a>
            <div class="languages">
              <a href="#">En</a>
              <a href="#">Ru</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header_middle_container">
      <div class="container">
        <div class="row">
          <div class="header_middle_box">
            <a href="index.php" class="logo">BOYA.AZ</a>
            <button class="hamburger"><img src="img/hamburger.svg" alt=""></button>
            <form action="">
              <input type="text" placeholder="Axtar...">
              <button type="submit" class="form_btn">Axtar</button>
            </form>
            <nav>
              <a href="#">Kateqoriyalar</a>
              <a href="#">Kampaniyalar</a>
              <a href="#">Ödəmə</a>
              <a href="#">Haqqımızda</a>
              <a href="#">Əlaqə</a>
              <div class="icons-right">
                <a href="">
                  <img src="img/love.svg" alt="">
                </a>
                <a href="">
                  <img src="img/shopping.svg" alt="">
                </a>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </div>
          
  </div>
  
</header>

<div class="header_overlay">
  <div class="overlay_loop">
    <div class="overlay_heading">
      <a href="index.php" class="logo">BOYA.AZ</a>
      <button class="overlay_close"><img src="img/esc.svg" alt=""></button>
    </div>

    <div class="company_header_box">
      <form action="">
        <input type="text" placeholder="Axtar...">
        <button type="submit" class="form_btn">Axtar</button>
      </form>

      <nav>
        <ul class="list-unstyled">
          <li><a href="#">Kampaniyalar</a></li>
          <li><a href="#">Ödəmə</a></li>
          <li><a href="#">Haqqımızda</a></li>
          <li><a href="#">Əlaqə</a></li>
        </ul>
      </nav>

      <div class="second_list">
        <a href="#">Çatdırılma</a>
        <a href="#">Tez-tez verilən suallar</a>
        <a href="#">Çağrı mərkəzi <span>*5111</span></a>
      </div>

      <div class="overlay_footer">
        <div class="over_footer_langs">
          <a href="#">Ru</a>
          <a href="#">En</a>
        </div>
      </div>
    </div>
    <div class="section_header_box">
      <ul class="main_ul">
          <li class="has-submenu-link">
            <div>
              <a href="#">Corella boyaları</a>
              <button class=""><img src="img/mobile-arrow.svg" alt=""></button>
            </div>
            <div class="submenu">
              <ul>
                <li><a href="#">Emulsiyalar</a></li>
                <li><a href="#">Sintetik boyalar</a></li>
                <li><a href="#">Sənaye boyaları</a></li>
                <li><a href="#">Laklar dolğular</a></li>
                <li><a href="#">Flexlər</a></li>
                <li><a href="#">Yapışkanlar</a></li>
              </ul>
            </div>
          </li>
          <li class="has-submenu-link">
            <div>
              <a href="#">Corella boyaları</a>
              <button class=""><img src="img/mobile-arrow.svg" alt=""></button>
            </div>
            <div class="submenu">
              <ul>
                <li><a href="#">Emulsiyalar</a></li>
                <li><a href="#">Sintetik boyalar</a></li>
                <li><a href="#">Sənaye boyaları</a></li>
                <li><a href="#">Laklar dolğular</a></li>
                <li><a href="#">Flexlər</a></li>
                <li><a href="#">Yapışkanlar</a></li>
              </ul>
            </div>
          </li>
          <li class="has-submenu-link">
            <div>
              <a href="#">Corella boyaları</a>
              <button class=""><img src="img/mobile-arrow.svg" alt=""></button>
            </div>
            <div class="submenu">
              <ul>
                <li><a href="#">Emulsiyalar</a></li>
                <li><a href="#">Sintetik boyalar</a></li>
                <li><a href="#">Sənaye boyaları</a></li>
                <li><a href="#">Laklar dolğular</a></li>
                <li><a href="#">Flexlər</a></li>
                <li><a href="#">Yapışkanlar</a></li>
              </ul>
            </div>
          </li>
          <li class="has-submenu-link">
            <div>
              <a href="#">Corella boyaları</a>
              <button class=""><img src="img/mobile-arrow.svg" alt=""></button>
            </div>
            <div class="submenu">
              <ul>
                <li><a href="#">Emulsiyalar</a></li>
                <li><a href="#">Sintetik boyalar</a></li>
                <li><a href="#">Sənaye boyaları</a></li>
                <li><a href="#">Laklar dolğular</a></li>
                <li><a href="#">Flexlər</a></li>
                <li><a href="#">Yapışkanlar</a></li>
              </ul>
            </div>
          </li>
          <li class="has-submenu-link">
            <div>
              <a href="#">Corella boyaları</a>
              <button class=""><img src="img/mobile-arrow.svg" alt=""></button>
            </div>
            <div class="submenu">
              <ul>
                <li><a href="#">Emulsiyalar</a></li>
                <li><a href="#">Sintetik boyalar</a></li>
                <li><a href="#">Sənaye boyaları</a></li>
                <li><a href="#">Laklar dolğular</a></li>
                <li><a href="#">Flexlər</a></li>
                <li><a href="#">Yapışkanlar</a></li>
              </ul>
            </div>
          </li>
          <li class="has-submenu-link">
            <div>
              <a href="#">Corella boyaları</a>
              <button class=""><img src="img/mobile-arrow.svg" alt=""></button>
            </div>
            <div class="submenu">
              <ul>
                <li><a href="#">Emulsiyalar</a></li>
                <li><a href="#">Sintetik boyalar</a></li>
                <li><a href="#">Sənaye boyaları</a></li>
                <li><a href="#">Laklar dolğular</a></li>
                <li><a href="#">Flexlər</a></li>
                <li><a href="#">Yapışkanlar</a></li>
              </ul>
            </div>
          </li>
          <li class="has-submenu-link">
            <div>
              <a href="#">Corella boyaları</a>
              <button class=""><img src="img/mobile-arrow.svg" alt=""></button>
            </div>
            <div class="submenu">
              <ul>
                <li><a href="#">Emulsiyalar</a></li>
                <li><a href="#">Sintetik boyalar</a></li>
                <li><a href="#">Sənaye boyaları</a></li>
                <li><a href="#">Laklar dolğular</a></li>
                <li><a href="#">Flexlər</a></li>
                <li><a href="#">Yapışkanlar</a></li>
              </ul>
            </div>
          </li>
      </ul>
    </div>

  </div>
</div>

<div class="tool_bar_mobile">
  <a href="" class="tool-link tool-active">
    <svg width="33" height="27" viewBox="0 0 33 27" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect width="22.718" height="22.718" rx="5" transform="matrix(-1 0 0 1 32.0762 0)" fill="#FBFDFF"/>
      <rect x="-0.75" y="0.75" width="21.218" height="21.218" rx="4.25" transform="matrix(-1 0 0 1 30.5762 0)" stroke="#2C343E" stroke-opacity="0.97" stroke-width="1.5"/>
      <rect width="22.718" height="22.718" rx="5" transform="matrix(-1 0 0 1 27.7487 1.79639)" fill="white"/>
      <rect x="-0.75" y="0.75" width="21.218" height="21.218" rx="4.25" transform="matrix(-1 0 0 1 26.2487 1.79639)" stroke="#2C343E" stroke-opacity="0.97" stroke-width="1.5"/>
      <rect width="22.718" height="22.718" rx="5" transform="matrix(-1 0 0 1 23.4218 3.59277)" fill="white"/>
      <rect x="-0.75" y="0.75" width="21.218" height="21.218" rx="4.25" transform="matrix(-1 0 0 1 21.9218 3.59277)" stroke="#2C343E" stroke-opacity="0.97" stroke-width="1.5"/>
    </svg>

    Kateqoriyalar
  </a>
  <button class="tool-link">
    <svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect x="-0.75" y="0.75" width="11.5645" height="11.5645" rx="1.25" transform="matrix(-1 0 0 1 11.5645 13.9355)" stroke="#2C343E" stroke-opacity="0.97" stroke-width="1.5"/>
      <rect x="-0.75" y="0.75" width="10.6935" height="11.5645" rx="1.25" transform="matrix(-1 0 0 1 25.5 13.9355)" stroke="#2C343E" stroke-opacity="0.97" stroke-width="1.5"/>
      <rect x="-0.75" y="0.75" width="10.6935" height="11.5645" rx="1.25" transform="matrix(-1 0 0 1 25.5 0)" stroke="#2C343E" stroke-opacity="0.97" stroke-width="1.5"/>
      <rect x="-0.75" y="0.75" width="11.5645" height="11.5645" rx="1.25" transform="matrix(-1 0 0 1 11.5645 0)" stroke="#2C343E" stroke-opacity="0.97" stroke-width="1.5"/>
    </svg>

    Bölmələr

  </button>
  <a href="favourites.php" class="tool-link"><img src="" alt="">
    <svg width="33" height="27" viewBox="0 0 33 27" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M16.5 27L15.9176 26.5455C3.39706 17 0 13.6364 0 8.18182C0 3.63636 3.88235 0 8.73529 0C12.7147 0 14.9471 2.09091 16.5 3.72727C18.0529 2.09091 20.2853 0 24.2647 0C29.1176 0 33 3.63636 33 8.18182C33 13.6364 29.6029 17 17.0824 26.5455L16.5 27ZM8.73529 1.81818C4.95 1.81818 1.94118 4.63636 1.94118 8.18182C1.94118 12.8182 5.04706 15.9091 16.5 24.6364C27.9529 15.9091 31.0588 12.8182 31.0588 8.18182C31.0588 4.63636 28.05 1.81818 24.2647 1.81818C20.8676 1.81818 19.0235 3.72727 17.5676 5.27273L16.5 6.45455L15.4324 5.27273C13.9765 3.72727 12.1324 1.81818 8.73529 1.81818Z" fill="#2C343E" fill-opacity="0.97"/>
    </svg>

    Bəyəndiklərim

  </a>
  <a href="basket.php" class="tool-link">
    <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M27.8289 7.5504C27.6549 7.29637 27.4212 7.16935 27.1281 7.16935H8.46549L7.03625 2.17339C6.90799 1.72177 6.73391 1.33602 6.51403 1.01613C6.29415 0.696237 6.06052 0.47043 5.81315 0.33871C5.56579 0.206989 5.35048 0.117608 5.16725 0.0705645C4.98401 0.0235215 4.80078 0 4.61754 0H0.852046C0.61384 0 0.41228 0.0846774 0.247368 0.254032C0.0824561 0.423387 0 0.639785 0 0.903226C0 1.05376 0.0366471 1.1996 0.109941 1.34073C0.183236 1.48185 0.288596 1.59005 0.426023 1.66532C0.56345 1.74059 0.705458 1.77823 0.852046 1.77823H4.61754C4.69083 1.77823 4.75955 1.78763 4.82368 1.80645C4.88781 1.82527 4.97485 1.90524 5.08479 2.04637C5.19473 2.1875 5.28635 2.39919 5.35965 2.68145L10.2246 21.0282C10.2612 21.1411 10.3208 21.2493 10.4032 21.3528C10.4857 21.4563 10.5819 21.5363 10.6918 21.5927C10.8017 21.6492 10.9208 21.6774 11.0491 21.6774H22.9228C23.106 21.6774 23.2755 21.621 23.4313 21.5081C23.587 21.3952 23.6924 21.254 23.7474 21.0847L27.9526 8.38306C28.0442 8.08199 28.003 7.80444 27.8289 7.5504ZM22.3181 19.871H11.7362L8.93274 8.97581H25.8637L22.3181 19.871ZM20.5865 23.4839C19.9819 23.4839 19.4642 23.705 19.0336 24.1472C18.603 24.5894 18.3877 25.121 18.3877 25.7419C18.3877 26.3629 18.603 26.8945 19.0336 27.3367C19.4642 27.7789 19.9819 28 20.5865 28C21.1912 28 21.7089 27.7789 22.1395 27.3367C22.5701 26.8945 22.7854 26.3629 22.7854 25.7419C22.7854 25.121 22.5701 24.5894 22.1395 24.1472C21.7089 23.705 21.1912 23.4839 20.5865 23.4839ZM12.6708 23.4839C12.2676 23.4839 11.8966 23.5874 11.5576 23.7944C11.2186 24.0013 10.9529 24.2742 10.7605 24.6129C10.5681 24.9516 10.4719 25.328 10.4719 25.7419C10.4719 26.3629 10.6872 26.8945 11.1178 27.3367C11.5484 27.7789 12.0661 28 12.6708 28C13.2754 28 13.7931 27.7789 14.2237 27.3367C14.6543 26.8945 14.8696 26.3629 14.8696 25.7419C14.8696 25.5914 14.8558 25.4409 14.8284 25.2903C14.8009 25.1398 14.7596 24.9987 14.7047 24.8669C14.6497 24.7352 14.581 24.6082 14.4985 24.4859C14.4161 24.3636 14.3245 24.2507 14.2237 24.1472C14.1229 24.0437 14.013 23.9496 13.8939 23.8649C13.7747 23.7802 13.6511 23.7097 13.5228 23.6532C13.3945 23.5968 13.2571 23.5544 13.1105 23.5262C12.9639 23.498 12.8173 23.4839 12.6708 23.4839Z" fill="#2C343E" fill-opacity="0.97"/>
    </svg>
    Səbət
  </a>
</div>


<div class="boya_mosaic" id="mosaicLoader">
  <img src="img/preloader.svg" alt="">
</div>
