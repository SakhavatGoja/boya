<?php
    include("includes/head.php");
?>


<section class="index">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
        <?php
            include("includes/category.list.php");
        ?>
        <div class="index-landing ">
          <div class="landing_left">
            <img src="img/left_img.png" alt="">
            <div class="left_absolute">
              <p class="heading_left">NB güvən simvoludur.</p>
              <p class="content_left">
                “NB Qrup” ASC Azərbaycan Kimya Sənayesi sahəsində uğurla fəaliyyət
                 göstərən və dinamik surətdə inkişaf edən şirkətdir.
                </p>
                <a href="#" class="see_more">Daha ətraflı</a>
            </div>
          </div>
          <div class="landing_right">
            <a href="#" class="right_rectangular">
              <div class="right_rec_abs">
                <img src="img/rectangle.png" alt="">
              </div>
              <p class="header_rec">Bütün məhsullara payız endirimi</p>
              <p class="discount_rec">25%</p>
            </a>
            <a href="#" class="right_rectangular">
              <div class="right_rec_abs">
                <img src="img/rectangle.png" alt="">
              </div>
              <p class="header_rec">Bütün məhsullara payız endirimi</p>
              <p class="discount_rec">25%</p>
            </a>
            <a href="#" class="right_rectangular">
              <div class="right_rec_abs">
                <img src="img/rectangle.png" alt="">
              </div>
              <p class="discount_rec">25%</p>
              <p class="header_rec">Bütün məhsullara payız endirimi</p>
            </a>
            <a href="#" class="right_rectangular">
              <div class="right_rec_abs">
                <img src="img/rectangle.png" alt="">
              </div>
              <p class="discount_rec">25%</p>

              <p class="header_rec">Bütün məhsullara payız endirimi</p>
            </a>
            <div class="landing-absolute-right-icons">
                <a href="#"><img src="img/fav_absolute.svg" alt=""></a>
                <a href="#"><img src="img/shop_absolute.svg" alt=""></a>
            </div>

          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="repeat_main_container">
              <div class="same_bottom_container same_swiper galeryBox">
                <div class="heading_container_same">
                    <p class="title_same_heading">Fərqli ehtiyac və istifadəyə uyğun funksional məhsullar</p>
                    <a href="#" class="see_all">Bütün məhsullara bax</a>
                </div>
                <div class="galery_main_boxes">
                    <a href="blog_inner.php" class="galery_same_box">
                        <div class="galery_img">
                            <img src="img/galery1.jpg" alt="">
                        </div>
                    </a>
                    <a href="blog_inner.php" class="galery_same_box">
                        <div class="galery_img">
                            <img src="img/galery2.jpg" alt="">
                        </div>
                    </a>
                    <a href="blog_inner.php" class="galery_same_box">
                        <div class="galery_img">
                            <img src="img/galery3.jpg" alt="">
                        </div>
                    </a>
                    <a href="blog_inner.php" class="galery_same_box">
                        <div class="galery_img">
                            <img src="img/galery4.jpg" alt="">
                        </div>
                    </a>
                    <a href="blog_inner.php" class="galery_same_box">
                        <div class="galery_img">
                            <img src="img/galery5.jpg" alt="">
                        </div>
                    </a>
                    <a href="blog_inner.php" class="galery_same_box">
                        <div class="galery_img">
                            <img src="img/galery6.jpg" alt="">
                        </div>
                    </a>
                    <a href="blog_inner.php" class="galery_same_box">
                        <div class="galery_img">
                            <img src="img/galery7.jpg" alt="">
                        </div>
                    </a>
                    <a href="blog_inner.php" class="galery_same_box">
                        <div class="galery_img">
                            <img src="img/galery8.jpg" alt="">
                        </div>
                    </a>
                    <a href="blog_inner.php" class="galery_same_box">
                        <div class="galery_img">
                            <img src="img/galery9.jpg" alt="">
                        </div>
                    </a>
                </div>
              </div>
              <div class="same_bottom_container same_swiper ">
                  <div class="heading_container_same">
                      <p class="title_same_heading">Fərqli ehtiyac və istifadəyə uyğun funksional məhsullar</p>
                      <a href="#" class="see_all">Bütün məhsullara bax</a>
                  </div>
                  <div class="swiper-container same_grid_boxes">
                      <div class="swiper-wrapper">
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="same_bottom_container same_swiper img-swiper">
                  <div class="heading_container_same">
                      <p class="title_same_heading">Fərqli ehtiyac və istifadəyə uyğun funksional məhsullar</p>
                      <a href="#" class="see_all">Bütün məhsullara bax</a>
                  </div>
                  <div class="swiper-container same_img_boxes">
                    <div class="swiper-wrapper">
                      <div class="swiper-slide">
                        <div class="swiper-slide-img"><img src="img/slide1.png" alt=""></div>
                      </div>
                      <div class="swiper-slide">
                        <div class="swiper-slide-img"><img src="img/slide2.png" alt=""></div>
                      </div>
                      <div class="swiper-slide">
                        <div class="swiper-slide-img"><img src="img/slide3.png" alt=""></div>
                      </div>
                    </div>
                  </div>
                  <div class="swiper-container same_grid_boxes">
                      <div class="swiper-wrapper">
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="same_bottom_container same_swiper f_swiper form-swiper">
                  <div class="heading_container_same">
                      <p class="title_same_heading">Fərqli ehtiyac və istifadəyə uyğun funksional məhsullar</p>
                      <a href="#" class="see_all">Bütün məhsullara bax</a>
                  </div>
                  <div class="swiper_4_boxes">
                    <div class="swiper-container four_swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column-right" >
                      <div class="column_form_container">
                        <h3 class="column_form_header">YENİLİKLƏRƏ ABUNƏ OL!</h3>
                        <p class="column_form_content">Yeniliklərə və kompaniyalara üzv olmaq üçün sadəcə mailini yazmaq yetərlidir!</p>
                        <form action="">
                          <input type="text" placeholder="Email Adresiniz" required>
                          <button type="submit" class="btn_blue">Üzv</button>
                        </form>
                        <div class="form_img"><img src="img/form_img.png" alt=""></div>
                      </div>
                    </div>
                  </div>
                  
              </div>
              <div class="same_bottom_container same_swiper">
                  <div class="heading_container_same">
                      <p class="title_same_heading">Fərqli ehtiyac və istifadəyə uyğun funksional məhsullar</p>
                      <a href="#" class="see_all">Bütün məhsullara bax</a>
                  </div>
                  <div class="swiper-container same_grid_boxes">
                      <div class="swiper-wrapper">
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="same_bottom_container same_swiper f_swiper left_four_swiper">
                  <div class="heading_container_same">
                      <p class="title_same_heading">Fərqli ehtiyac və istifadəyə uyğun funksional məhsullar</p>
                      <a href="#" class="see_all">Bütün məhsullara bax</a>
                  </div>
                  <div class="swiper_4_boxes">
                    <div class="swiper-container four_swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column-right" >
                      <div class="column_inner_box" style="background: url('img/road.png') center/cover no-repeat"></div>
                    </div>
                  </div>
                  
              </div>
              <div class="same_bottom_container same_swiper">
                  <div class="heading_container_same">
                      <p class="title_same_heading">Fərqli ehtiyac və istifadəyə uyğun funksional məhsullar</p>
                      <a href="#" class="see_all">Bütün məhsullara bax</a>
                  </div>
                  <div class="swiper-container same_grid_boxes">
                      <div class="swiper-wrapper">
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="same_bottom_container same_swiper f_swiper">
                  <div class="heading_container_same">
                      <p class="title_same_heading">Fərqli ehtiyac və istifadəyə uyğun funksional məhsullar</p>
                      <a href="#" class="see_all">Bütün məhsullara bax</a>
                  </div>
                  <div class="swiper_4_boxes">
                    <div class="swiper-container four_swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column-right" >
                      <div class="column_inner_box" style="background: url('img/road.png') center/cover no-repeat"></div>
                    </div>
                  </div>
                  
              </div>
              <div class="same_bottom_container colors_box_container">
                <div class="heading_container_same">
                    <p class="title_same_heading">Fərqli ehtiyac və istifadəyə uyğun funksional məhsullar</p>
                    <a href="#" class="see_all">Bütün məhsullara bax</a>
                </div>
                <div class="img_grid_main">
                  <div class="colored_buttons">
                    <div class="colored_box">
                      <a class="colored_link"  href="mutualPage.php">Sintetik boyalar</a>
                    </div>
                    <div class="colored_box">
                      <a class="colored_link"  href="mutualPage.php">Sintetik boyalar</a>
                    </div>
                    <div class="colored_box">
                      <a class="colored_link"  href="mutualPage.php">Sintetik boyalar</a>
                    </div>
                    <div class="colored_box">
                      <a class="colored_link"  href="mutualPage.php">Sintetik boyalar</a>
                    </div>
                    <div class="colored_box">
                      <a class="colored_link"  href="mutualPage.php">Sintetik boyalar</a>
                    </div>
                    <div class="colored_box">
                      <a class="colored_link"  href="mutualPage.php">Sintetik boyalar</a>
                    </div>
                  </div>

                  <div class="imgages_paint_galery">
                      <div class="image_galery_single"><img src="img/image1.png" alt=""></div>
                      <div class="image_galery_single"><img src="img/image2.png" alt=""></div>
                      <div class="image_galery_single"><img src="img/image3.png" alt=""></div>
                      <div class="image_galery_single"><img src="img/image4.png" alt=""></div>
                  </div>
                </div>
              </div>
              <div class="same_bottom_container same_swiper img-swiper">
                  <div class="heading_container_same">
                      <p class="title_same_heading">Fərqli ehtiyac və istifadəyə uyğun funksional məhsullar</p>
                      <a href="#" class="see_all">Bütün məhsullara bax</a>
                  </div>
                  <div class="swiper-container same_img_boxes">
                    <div class="swiper-wrapper">
                      <div class="swiper-slide">
                        <div class="swiper-slide-img"><img src="img/slide1.png" alt=""></div>
                      </div>
                      <div class="swiper-slide">
                        <div class="swiper-slide-img"><img src="img/slide2.png" alt=""></div>
                      </div>
                      <div class="swiper-slide">
                        <div class="swiper-slide-img"><img src="img/slide3.png" alt=""></div>
                      </div>
                    </div>
                  </div>
                  <div class="swiper-container same_grid_boxes">
                      <div class="swiper-wrapper">
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="same_bottom_container same_swiper f_swiper left_four_swiper">
                  <div class="heading_container_same">
                      <p class="title_same_heading">Fərqli ehtiyac və istifadəyə uyğun funksional məhsullar</p>
                      <a href="#" class="see_all">Bütün məhsullara bax</a>
                  </div>
                  <div class="swiper_4_boxes">
                    <div class="swiper-container four_swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="paint_box">
                                    <div class="discount">
                                        <p>45%</p>
                                    </div>
                                    <button class="heart">
                                        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                        </svg>
                                    </button>
                                    <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                    <div class="paint_desc">
                                        <p class="paint_firm">Corella boyaları</p>
                                        <p class="paint_name">Emulsiyalar</p>
                                        <p class="paint_type">Super Plus</p>
                                    </div>
                                    <div class="paint_prices">
                                        <div class="price_box">
                                            <p class="current_price">75.80₼</p>
                                            <p class="old_price">63.50₼</p>
                                        </div>
                                        <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column-right" >
                      <div class="column_inner_box" style="background: url('img/road.png') center/cover no-repeat"></div>
                    </div>
                  </div>
                  
              </div>
              <div class="same_bottom_container same_swiper">
                  <div class="heading_container_same">
                      <p class="title_same_heading">Fərqli ehtiyac və istifadəyə uyğun funksional məhsullar</p>
                      <a href="#" class="see_all">Bütün məhsullara bax</a>
                  </div>
                  <div class="swiper-container same_grid_boxes">
                      <div class="swiper-wrapper">
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                          <div class="swiper-slide">
                              <div class="paint_box">
                                  <div class="discount">
                                      <p>45%</p>
                                  </div>
                                  <button class="heart">
                                      <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M9.5 16L9.16471 15.7306C1.95588 10.0741 0 8.08081 0 4.84848C0 2.15488 2.23529 0 5.02941 0C7.32059 0 8.60588 1.23906 9.5 2.20875C10.3941 1.23906 11.6794 0 13.9706 0C16.7647 0 19 2.15488 19 4.84848C19 8.08081 17.0441 10.0741 9.83529 15.7306L9.5 16ZM5.02941 1.07744C2.85 1.07744 1.11765 2.74747 1.11765 4.84848C1.11765 7.59596 2.90588 9.42761 9.5 14.5993C16.0941 9.42761 17.8824 7.59596 17.8824 4.84848C17.8824 2.74747 16.15 1.07744 13.9706 1.07744C12.0147 1.07744 10.9529 2.20875 10.1147 3.12458L9.5 3.82492L8.88529 3.12458C8.04706 2.20875 6.98529 1.07744 5.02941 1.07744Z" fill="#2F76C1"/>
                                      </svg>
                                  </button>
                                  <div class="paint_img"><img src="img/paint.jpg" alt=""></div>
                                  <div class="paint_desc">
                                      <p class="paint_firm">Corella boyaları</p>
                                      <p class="paint_name">Emulsiyalar</p>
                                      <p class="paint_type">Super Plus</p>
                                  </div>
                                  <div class="paint_prices">
                                      <div class="price_box">
                                          <p class="current_price">75.80₼</p>
                                          <p class="old_price">63.50₼</p>
                                      </div>
                                      <button type="button" class="paint_basket"><img src="img/basket.svg" alt=""></button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
