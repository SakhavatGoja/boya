<?php
    include("includes/head.php");
?>


<section class="catalog">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
        <?php
            include("includes/category.list.php");
        ?>
        <div class="container">
          <div class="row">
              <div class="catalog_main_container">
                  <div class="breadcrumbs">
                      <span class="old_page">Azclimart</span>
                      <img src="img/breadcrumb.svg" alt="">
                      <a href="#" class="new_page">Ral kataloqları</a>
                  </div>

                  <div class="heading_container_same">
                      <p class="title_same_heading">Ral kataloqları</p>
                  </div>

                  <div class="catalog_info_container">
                    <p class="cat_desc">
                      Sənaye boyalarından istifadə etmək üçün mütəxəssislərimizdən məhsul haqqında ətraflı məlumat əldə etdikdən sonra istəyinizə
                      uyğun rəng seçimi edə bilərsiniz.
                    </p>
                    <div class="catalog_box">
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #8866aa">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #225511">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #55ff66">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #336688">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #33cc44">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #040021">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #33ee88">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #55ff44">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #226622">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #f50407">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #dddddd">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #4f3638">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #20b2aa">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #060757">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #775948">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #510000">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #dddddd">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #4f3638">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #5d5451">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #8d837a">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #900000">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #543515">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #2F76C1">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                      <div class="catalog_same">
                        <div class="catalog_one" style="background-color: #6F767C">
                        </div>
                        <p>RAL 9669</p>
                      </div>
                    </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
