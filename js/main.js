// basket-start
sum();
function sum(){
  var total = 0;
  let disPrice = parseInt($('.basket_grid input.delivery_price_input').val());
  $('.basket_total_boxes .basket_single_box').each(function(index ,item ){
    let totalPrice = $(item).find('.basket_product_operation .money_price')
    total += +parseFloat($(totalPrice).text());
    let length = $('.basket_total_boxes .basket_single_box').length
    $('.basket_count #count_page').text(length)
  });
  if(total > 200){
    if($(".result_box #total").hasClass('with-delivery')) {
      $(".result_box #total").removeClass('with-delivery');
    }
  }
  else{
    total +=disPrice;
    $('.result_box #total').addClass('with-delivery');
  }
    
  $(".result_box #total").text(parseFloat(total).toFixed(2));
}

$(document).on('click','.basket_count_box button', function(e) {
  e.preventDefault();
  var $this = $(this);
  let $item = $(this).parents('.basket_product_operation').find('.money_price').find('span');
  let $itemPrice = parseFloat($item.data('price'))
  var target = $this.parent().data('target');
  var target = $('#' + target);
  var current = parseFloat($(target).val());
  if ($this.hasClass('cart-plus-1')){
        target.val(current + 1);
        $item.text(parseFloat($itemPrice * target.val()).toFixed(2))
      }
  else {
    if(current < 2){
      return null
    }
    else{
      target.val(current - 1);
      $item.text(parseFloat($itemPrice * target.val()).toFixed(2))
    }
  }

  sum()
  
});

$(document).on('input' , '.basket_count_box input', function(e) {
  var $this = $(this);
  let $item = $(this).parents('.basket_product_operation').find('.money_price').find('span');
  let $itemPrice = parseFloat($item.data('price'))
  var target = $this.parents('.basket_count_box').data('target');
  var target = $('#' + target);
  var current = parseFloat($(target).val());
  $item.text(parseFloat($itemPrice * target.val()).toFixed(2))
  sum();
});

$(document).on('click','.basket_single_box .delete',function(){
  let $parent = $(this).parents('.basket_single_box');
  $parent.fadeOut(300, function(){
       $parent.remove();
       sum();
  })
  if($('.basket_grid .basket_single_box').length < 2) setTimeout(() => {
    $('.result_box').remove()
    $('.basket_count #count_page').text('0')
  }, 300);
})

let i = 0;
$('.paint_basket').on('click', function() {
  i+=1;
  let basket_left = `
    <div class="basket_left">
      <div class="basket_product_img"><img src="img/basket_paint.jpg" alt=""></div>
      <div class="basket_product_info">
        <div class="basket_product_name">
          <p>Parket tikurella lakı</p>
        </div>
        <p class="basket_product_code">Məhsul kodu :<span>00${i}</span></p>
      </div>
      <div class="basket_product_operation">
        <div class="basket_count_box" data-target="amount-${i}">
          <button class="cart-minus-1"><img src="img/minus.svg" alt=""></button>
          <input type="number" id="amount-${i}" value="1" name="" min="1">
          <button class="cart-plus-1"><img src="img/plus.svg" alt=""></button>
        </div>
        <p class="money_price" ><span data-price="75.80">75.80</span>₼</p>
      </div>
    </div>
  `;

  let delimg = $('<img />', { 
    src: "img/esc.svg"
  });;
  let delBtn = $('<button />', { 
    class:'delete',
    id:i
  });;
  let singleBox = $('<div />', { 
    class:'basket_single_box',
    id:i
  });
  $(basket_left).appendTo(singleBox);
  delimg.appendTo(delBtn);
  delBtn.appendTo(singleBox);
  singleBox.appendTo('#addBasket .modal-body')
  setTimeout(() => {
    $('#addBasket').modal('show');
  }, 350);   
});

// basket-end


// tabs-start

$('.project_heading_container ul li button').on('click',function(){
  var data = $(this).data('tab');
  var div = $('.project_filtered_boxes .item_standart');
  $('.project_heading_container ul li button').removeClass('active_link');
  $(this).addClass('active_link');
  $('#loading').show();
	$('.projects_container').css('opacity','.1');
	setTimeout(function(){
		$('#loading').hide();
		$('.projects_container').css('opacity','unset');
	},600)
  if(!data){
      div.css('display','flex');
  }
  else{
      div.css('display','none');
      for(i=0;i<div.length;i++){
          var dataId = $(div[i]).data('id');
          if (data==dataId) $(div[i]).css('display','flex'); 
      }
  }    
})
// tabs-end

// favourites-start
$('.favourite_single .delete').on('click',function(){
  let $parentFav = $(this).parents('.favourite_single')
  $parentFav.fadeOut(300, function(){
    $parentFav.remove();
  })
})
// favourites-end


// slider-range


// basket inner start
var innerBtn = $('.inner_count_box').find('button');

$(innerBtn).on('click', function(e) {
  e.preventDefault();
  var $this = $(this);
  let $item = $(this).parents('.project_inner_operation').find('span.price');
  let $itemPrice = parseFloat($item.data('price')).toFixed(2)
  var target = $this.parent().data('target');
  var target = $('#' + target);
  var current = parseFloat($(target).val());
  if ($this.hasClass('plus')){
        target.val(current + 1);
        $item.text(parseFloat($itemPrice * target.val()).toFixed(2))
      }
  else {
    if(current < 2){
      return null
    }
    else{
      target.val(current - 1);
      $item.text(parseFloat($itemPrice * target.val()).toFixed(2))
    }
  }
});

$('.inner_count_box input').on('input',function(){
  var $this = $(this);
  let $item = $(this).parents('.project_inner_operation').find('span.price');
  let $itemPrice = parseFloat($item.data('price'))
  var target = $this.parents('.inner_count_box').data('target');
  var target = $('#' + target);
  var current = parseFloat($(target).val());
  $item.text(parseFloat($itemPrice * target.val()).toFixed(2))
})
// basket inner end


// input range slider filter start
$(".js-range-slider").ionRangeSlider({
    type: "double",
    min: 120,
    max: 7000,
    skin: "big",
    keyboard: true,
    force_edges: false
  }
)
let my_range = $(".js-range-slider").data("ionRangeSlider")
// input range slider filter end

// label filter start
$('.label_single input').on('change',function(){
  if($(this).is(':checked')){
    $(this).attr('checked' , 'true')
  }
  else{
    $(this).removeAttr('checked')
  }
})
// label filter end

// clean filter start
$('#clean').on('click', function(e){
  e.preventDefault();
  my_range.reset();
  $('.brand_links a').removeClass('active');
  $('.label_single input').each(function() {
    if($(this).attr('checked')){
        $(this).prop("checked", false).removeAttr('checked');
      }
  });
})
// clean filter end


// hamburger - start
$('.hamburger_link').on('click',function(e){
  e.preventDefault();
  $('.header_overlay').addClass('open');
  $('.overlay_loop').find('nav , .overlay_footer').show()
  $('.overlay_loop').find('form').hide()
  $('body').css('overflow', 'hidden');
})
// hamburger - end

// overlay  - start
$('.overlay_close').on('click', function(){
  $('.header_overlay').removeClass('open')
  setTimeout(() => {
    $('body').css('overflow', 'unset');
  }, 500);
}) 
// overlay - end

// search-mobile-start
$('.search_mobile').on('click',function(e){
  e.preventDefault();
  $('.header_overlay').addClass('open');
  $('.overlay_loop').find('nav , .overlay_footer').hide()
  $('.overlay_loop').find('form').show()
  $('body').css('overflow', 'hidden');
})
// search-mobile-end

// mobile-accordion-start
$('.list-unstyled .has-submenu-link button').on('click', function(){
  $(this).toggleClass('rotate');
  $(this).parents('.has-submenu-link').find('.submenu').slideToggle()
})
// mobile-accordion-end


// compare.btn start
$('.left_btns button.btn_blue').on('click',function(){
  $(this).parents('.compared_single_box').find('.compared_bottom').toggle(200)
})
// compare.btn end

// filter mobile start
$('button.filter').on('click', function(){
  $('.filter_grid aside').addClass('open')
})
$('.filter_grid aside button.esc').on('click',function(){
  $('.filter_grid aside').removeClass('open')
})
$('body').on('click', function (e) {
  if ($('aside.aside-form').length !== 0
      && !$(e.target).hasClass('aside-form')       
      && !$(e.target).parents('.aside-form').length 
      && !$(e.target).hasClass('filter')
      && !$(e.target).parents('.filter').length
      ){
     $('.filter_grid aside').removeClass('open')
  }
});
// filter mobile end

// stars assuming start
$('.feedback_rating li').on('click',function() {
  let rated = $(this);
  let dataStar = rated.data('star');
  rated.addClass('rated');
  $(rated).prevAll().addClass('rated');
  $(rated).nextAll().removeClass('rated');
  $('input[name="star"]').val(dataStar);
})
// stars assuming end


const catalogBox = document.querySelectorAll('.catalog_same')
catalogBox && catalogBox.forEach(item =>{
  let text = item.querySelector('p')
  let rgb = item.querySelector('.catalog_one').style.backgroundColor;
  let black = 'rgba(44, 52, 62, 0.97)';
  let white = 'rgba(255, 255, 255, 0.97)';
  rgb = rgb.substring(4, rgb.length-1)
         .replace(/ /g, '')
         .split(',');
  
  const brightness = Math.round(((parseInt(rgb[0]) * 299) +
            (parseInt(rgb[1]) * 587) +
            (parseInt(rgb[2]) * 114)) / 1000);
  
  console.log(brightness)
  const textColour = (brightness > 125) ? `${black}` : `${white}`;
  text.style.color = textColour
})

// hamburger - start
$('.hamburger').on('click',function(e){
  e.preventDefault();
  $('.header_overlay').addClass('open');
  $('.overlay_loop').find('.company_header_box').show()
  $('.overlay_loop').find('.section_header_box').hide()

  $('body').css('overflow', 'hidden');
})
// hamburger - end

// overlay  - start
$('.overlay_close').on('click', function(){
  $('.header_overlay').removeClass('open')
  setTimeout(() => {
    $('body').css('overflow', 'unset');
  }, 500);
}) 
// overlay - end

$('button.tool-link').on('click',function(){
  $('.tool-link').removeClass('tool-active');
  $(this).addClass('tool-active');
  $('.header_overlay').addClass('open');
  $('.overlay_loop').find('.company_header_box').hide()
  $('.overlay_loop').find('.section_header_box').show()
  $('body').css('overflow', 'hidden');
})

$('.section_header_box .has-submenu-link button').on('click', function(){
  $(this).toggleClass('rotate');
  $(this).parents('.has-submenu-link').find('.submenu').slideToggle()
})


$('.project_top_container .swiper-slide').on('click',function(){
  $('#imgShow').find('.modal-img').find('img').attr('src', $(this).find('.project_loop_box').find('img').attr('src'));
  setTimeout(() => {
    $('#imgShow').modal('show');
  }, 350);
})

$('.project_top_container .one_img_box').on('click' ,function(){
  $('#imgShow').find('.modal-img').find('img').attr('src', $(this).find('img').attr('src'));
  setTimeout(() => {
    $('#imgShow').modal('show');
  }, 350);
})

$('.heart').on('click',function(){
  let favSec = $('.favourites').length;
  if(favSec && $(this).parents('.favourites')){
    if($(this).hasClass('active')){
      let $parentFav = $(this).parents('.favourite_single')
      $parentFav.fadeOut(300, function(){
        $parentFav.remove();
      })
    }
  }
  $(this).toggleClass('active')
})


// project_inner-start
$('button.size_one').on('click',function(){
  $('button.size_one').removeClass('active');
  $(this).addClass('active');
})
// project_inner-end

// gif animated
var loader = document.querySelector('.boya_mosaic')

window.addEventListener('load',vanish);

function vanish(){
    setTimeout(() => {
      loader.classList.add('disappear')
    }, 650);
}