<?php
    include("includes/head.php");
?>


<section class="blog">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
        <?php
            include("includes/category.list.php");
        ?>
        <div class="container">
            <div class="row">
                <div class="blog_main_container">
                    <div class="breadcrumbs">
                        <span class="old_page">Azclimart</span>
                        <img src="img/breadcrumb.svg" alt="">
                        <a href="#" class="new_page">Məkana özəl məhsullarımız</a>
                    </div>
                    <div class="blog_galery_container">
                        <div class="heading_container_same">
                            <p class="title_same_heading">Məkana özəl məhsullarımız</p>
                        </div>
                        <div class="galery_main_boxes">
                          <a href="blog_inner.php" class="galery_same_box">
                            <div class="galery_img">
                              <img src="img/galery1.jpg" alt="">
                            </div>
                          </a>
                          <a href="blog_inner.php" class="galery_same_box">
                            <div class="galery_img">
                              <img src="img/galery2.jpg" alt="">
                            </div>
                          </a>
                          <a href="blog_inner.php" class="galery_same_box">
                            <div class="galery_img">
                              <img src="img/galery3.jpg" alt="">
                            </div>
                          </a>
                          <a href="blog_inner.php" class="galery_same_box">
                            <div class="galery_img">
                              <img src="img/galery4.jpg" alt="">
                            </div>
                          </a>
                          <a href="blog_inner.php" class="galery_same_box">
                            <div class="galery_img">
                              <img src="img/galery5.jpg" alt="">
                            </div>
                          </a>
                          <a href="blog_inner.php" class="galery_same_box">
                            <div class="galery_img">
                              <img src="img/galery6.jpg" alt="">
                            </div>
                          </a>
                          <a href="blog_inner.php" class="galery_same_box">
                            <div class="galery_img">
                              <img src="img/galery7.jpg" alt="">
                            </div>
                          </a>
                          <a href="blog_inner.php" class="galery_same_box">
                            <div class="galery_img">
                              <img src="img/galery8.jpg" alt="">
                            </div>
                          </a>
                          <a href="blog_inner.php" class="galery_same_box">
                            <div class="galery_img">
                              <img src="img/galery9.jpg" alt="">
                            </div>
                          </a>
                        </div>
                        <a href="#" class="see_more">Daha çox</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
